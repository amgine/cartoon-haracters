# Characters

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn test`

## Environment variables

`REACT_APP_BASE_URL` - base url for getting characters (https://rickandmortyapi.com/api)

## Demo

https://characters-20e91.web.app/
