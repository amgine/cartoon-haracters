import React from 'react';
import { useSelector } from 'react-redux';

import { appState } from 'store/modules/app/selectors';
import Root from 'pages';
import Loader from 'components/shared/loader';
import useDidMount from 'hooks/useDidMount';
import { loadResources } from 'store/modules/app/actions';

const RootRouter = () => {
  const { isInit, isLoading } = useSelector(appState);
  useDidMount(loadResources);

  return (
    <>
      {isInit && <Root />}
      <Loader isLoading={isLoading} />
    </>
  );
};

export default RootRouter;
