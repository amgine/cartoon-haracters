export const ROUTES = {
  ROOT: '/',
  DOCS: '#',
  ABOUT: '#',
  HELP_US: '#',
};

const PAGE_NAMES = {
  DOCS: 'Docs',
  ABOUT: 'About',
  HELP_US: 'Help us',
};

export const PAGES = [
  { id: '123in', path: ROUTES.DOCS, name: PAGE_NAMES.DOCS },
  { id: '123iq', path: ROUTES.ABOUT, name: PAGE_NAMES.ABOUT },
  { id: '123iw', path: ROUTES.HELP_US, name: PAGE_NAMES.HELP_US },
];
