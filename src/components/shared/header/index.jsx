import React from 'react';
import { Link } from 'react-router-dom';

import { ROUTES, PAGES } from 'router/constants';

import s from './styles.module.scss';

const Header = () => (
  <header className={s.header}>
    <nav className={s.nav}>
      <Link to={ROUTES.ROOT} className={s.logo}>
        LOGO
      </Link>
      <ul className={s.linkList}>
        {PAGES.map((item) => (
          <li key={item.id} className={s.linkItem}>
            <Link key={item.id} to={item.path} className={s.link}>
              {item.name}
            </Link>
          </li>
        ))}
      </ul>
    </nav>
  </header>
);

export default Header;
