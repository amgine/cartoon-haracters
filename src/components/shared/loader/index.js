import React, { memo } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

import s from './styles.module.scss';

const Loader = ({ isLoading, position }) => {
  let content = null;
  if (isLoading) {
    content = (
      <div className={cx(s.root, s[position])}>
        <div className={s.ldsDefault}>
          <div />
          <div />
          <div />
          <div />
          <div />
          <div />
          <div />
          <div />
          <div />
          <div />
          <div />
          <div />
        </div>
      </div>
    );
  }

  return content;
};

Loader.defaultProps = {
  position: 'center',
};

Loader.propTypes = {
  isLoading: PropTypes.bool.isRequired,
  position: PropTypes.string,
};

export default memo(Loader);
