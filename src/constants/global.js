export const RESOURCES = {
  CHARACTERS: 'characters',
  LOCATIONS: 'locations',
  EPISODES: 'episodes',
};

export const STATUSES = {
  ALIVE: 'alive',
  DEAD: 'dead',
  UNKNOWN: 'unknown',
};
