import React from 'react';
import PropTypes from 'prop-types';

import Header from 'components/shared/header';
import Footer from 'components/shared/footer';

const Layout = ({ children }) => (
  <>
    <Header />
    {children}
    <Footer />
  </>
);

Layout.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Layout;
