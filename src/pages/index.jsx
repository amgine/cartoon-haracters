import React from 'react';
import { Route, Switch } from 'react-router-dom';

import { ROUTES } from 'router/constants';

import Layout from 'containers/layout';
import Home from './home';

const Root = () => (
  <Layout>
    <Switch>
      <Route exact path={ROUTES.ROOT} component={Home} />
    </Switch>
  </Layout>
);

export default Root;
