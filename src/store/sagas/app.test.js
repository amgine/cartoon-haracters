import sagaHelper from 'redux-saga-testing';
import { put, select, call, fork, all } from 'redux-saga/effects';

import {
  setChangeFilter,
  setCharacters,
  setIsLoadingCharacters,
  setIsLoading,
  loadResourcesSuccess,
  setLocations,
} from '../modules/app/actions';
import { getCharacterAllPages, getCharacterPage } from '../modules/app/selectors';
import { changeFilter, getCharacters, loadResources, getLocations, getEpisodes } from './app';
import { getCharactersApi, getLocationsApi } from '../../api/app';

describe('Start getCharacters saga', () => {
  const it = sagaHelper(getCharacters());

  it('start loading of characters', (result) => {
    expect(result).toEqual(put(setIsLoadingCharacters(true)));
  });

  it('start get page', (result) => {
    expect(result).toEqual(select(getCharacterPage));
    return 1;
  });

  it('get characters api', (result) => {
    const params = { page: 1 };
    expect(result).toEqual(call(getCharactersApi, params));

    return {
      info: { count: 591, pages: 30 },
      results: [],
    };
  });

  it('set characters', (result) => {
    expect(result).toEqual(
      put(setCharacters({ characterCount: 591, characters: [], characterAllPages: 30 })),
    );
  });

  it('finish loading', (result) => {
    expect(result).toEqual(put(setIsLoadingCharacters(false)));
  });
});

describe('Start changeFilter saga', () => {
  const it = sagaHelper(changeFilter());

  it('get all pages', (result) => {
    expect(result).toEqual(select(getCharacterAllPages));

    return 30;
  });

  it('start get page', (result) => {
    expect(result).toEqual(select(getCharacterPage));
    return 1;
  });

  it('set characterPage', (result) => {
    expect(result).toEqual(put(setChangeFilter({ characterPage: 2 })));
  });

  it('fork getCharacters', (result) => {
    expect(result).toEqual(fork(getCharacters));
  });
});

describe('Start loadResources saga', () => {
  const it = sagaHelper(loadResources());

  it('set setIsLoading to true', (result) => {
    expect(result).toEqual(put(setIsLoading(true)));
  });

  it('get data', (result) => {
    expect(result).toEqual(all([getCharacters(), getLocations(), getEpisodes()]));
  });

  it('load resources', (result) => {
    expect(result).toEqual(put(loadResourcesSuccess()));
  });

  it('set setIsLoading to false', (result) => {
    expect(result).toEqual(put(setIsLoading(false)));
  });
});

describe('Start getLocations saga', () => {
  const it = sagaHelper(getLocations());

  it('get locations api', (result) => {
    expect(result).toEqual(call(getLocationsApi));

    return {
      info: { count: 100 },
    };
  });

  it('set locations', (result) => {
    expect(result).toEqual(put(setLocations({ locationCount: 100 })));
  });
});
