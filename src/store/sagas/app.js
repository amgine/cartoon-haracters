import { all, select, put, takeLatest, call, fork } from 'redux-saga/effects';

import {
  loadResources as loadResourcesAction,
  loadResourcesFailure,
  loadResourcesSuccess,
  setIsLoading,
  setCharacters,
  setCharactersError,
  setLocations,
  setLocationsError,
  changeFilter as changeFilterAction,
  setChangeFilter,
  setChangeFilterError,
  setIsLoadingCharacters,
  setEpisodes,
  setEpisodesError,
} from 'store/modules/app/actions';
import { getCharactersApi, getLocationsApi, getEpisodesApi } from 'api/app';
import { getCharacterPage, getCharacterAllPages } from 'store/modules/app/selectors';

export function* getCharacters() {
  try {
    yield put(setIsLoadingCharacters(true));
    const page = yield select(getCharacterPage);

    const res = yield call(getCharactersApi, { page });

    const {
      info: { count, pages },
      results,
    } = res;

    yield put(
      setCharacters({ characterCount: count, characters: results, characterAllPages: pages }),
    );
  } catch (error) {
    yield put(setCharactersError());
  } finally {
    yield put(setIsLoadingCharacters(false));
  }
}

export function* changeFilter() {
  try {
    const allPages = yield select(getCharacterAllPages);
    let characterPage = yield select(getCharacterPage);
    characterPage += 1;

    if (characterPage <= allPages) {
      yield put(setChangeFilter({ characterPage }));
      yield fork(getCharacters);
    }
  } catch (error) {
    yield put(setChangeFilterError({ error: error.message }));
  }
}

export function* getLocations() {
  try {
    const res = yield call(getLocationsApi);

    const {
      info: { count: locationCount },
    } = res;

    yield put(setLocations({ locationCount }));
  } catch (error) {
    yield put(setLocationsError());
  }
}

export function* getEpisodes() {
  try {
    const res = yield call(getEpisodesApi);
    const {
      info: { count: episodeCount },
    } = res;
    yield put(setEpisodes({ episodeCount }));
  } catch (error) {
    yield put(setEpisodesError());
  }
}

export function* loadResources() {
  try {
    yield put(setIsLoading(true));

    yield all([getCharacters(), getLocations(), getEpisodes()]);
    yield put(loadResourcesSuccess());
  } catch (error) {
    yield put(loadResourcesFailure({ error: error.message }));
  } finally {
    yield put(setIsLoading(false));
  }
}

export default function* root() {
  yield takeLatest(loadResourcesAction, loadResources);
  yield takeLatest(changeFilterAction, changeFilter);
}
