import { createSelector } from 'reselect';

export const appState = (state) => state.app;

export const getCharacterPage = createSelector(appState, (state) => state.characterPage);
export const getCharacterAllPages = createSelector(appState, (state) => state.characterAllPages);
