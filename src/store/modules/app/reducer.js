import { handleActions } from 'redux-actions';

import {
  loadResourcesFailure,
  loadResourcesSuccess,
  setIsLoading,
  setCharacters,
  setCharactersError,
  setLocations,
  setLocationsError,
  setChangeFilter,
  setChangeFilterError,
  setIsLoadingCharacters,
  setEpisodes,
  setEpisodesError,
} from './actions';

export const defaultState = {
  isLoading: false,
  isInit: false,
  error: null,

  characterCount: null,
  characterAllPages: null,
  characters: [],
  characterPage: 1,
  characterPageError: null,
  isLoadingCharacters: false,

  locationCount: null,

  episodeCount: null,
  episodes: [],
};

const reducer = handleActions(
  {
    [setIsLoading]: (state, { payload: isLoading }) => ({
      ...state,
      isLoading,
    }),
    [loadResourcesSuccess]: (state) => ({
      ...state,
      isInit: true,
    }),
    [loadResourcesFailure]: (state, { payload: { error } }) => ({
      ...state,
      isInit: false,
      error,
    }),
    [setCharacters]: (state, { payload: { characterCount, characterAllPages, characters } }) => ({
      ...state,
      characterCount,
      characterAllPages,
      characters: [...state.characters, ...characters],
    }),
    [setCharactersError]: (state) => ({
      ...state,
      characterCount: null,
      characterAllPages: null,
    }),
    [setChangeFilter]: (state, { payload: { characterPage } }) => ({
      ...state,
      characterPage,
    }),
    [setChangeFilterError]: (state, { payload: { error } }) => ({
      ...state,
      characterPageError: error,
    }),
    [setLocations]: (state, { payload: { locationCount } }) => ({
      ...state,
      locationCount,
    }),
    [setLocationsError]: (state) => ({
      ...state,
      locationCount: null,
    }),
    [setEpisodes]: (state, { payload: { episodeCount } }) => ({
      ...state,
      episodeCount,
    }),
    [setEpisodesError]: (state) => ({
      ...state,
      episodeCount: null,
    }),
    [setIsLoadingCharacters]: (state, { payload: isLoadingCharacters }) => ({
      ...state,
      isLoadingCharacters,
    }),
  },

  defaultState,
);

export default reducer;
